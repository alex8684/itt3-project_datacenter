---
title: '20A ITT3 Project'
subtitle: 'Project plan'
authors: 
    * Alexander With 
    * Mikkel Krogh Hansen 
    * Cristian Guba 
    * Jacob Pløger 
    * Franco Mariani Westi
    * Lasse Didrichsen
    * Mateusz Pietrzyk 
    * Nicolai Skytte
    * Jonas Offersen
    * 
main_author: 'Group Datacenter'
date: \today
email: ''
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---

# 20A ITT3 Project
This project is a cooperation between various partners. UCL, Fredericia Maskinmesterskole, NNIT, Danish Data Center Industry, Data Center Grupper and Google.
## Background
"Start with the end in mind”: It’s something everyone agrees on, but it’s always set aside for one reason or another (Typical Economy)

Datacenter A/S is in the process of establishing a new datacenter called Datacamp NOW for a customer. Datacenter A/S is planning and developing the new datacenter to be launched on February 1, 2021. In DataCamp NOW, a new data center unit with X number of servers (several specs missing) will be built,which will provide Tb space based on given quality goals. (More details are missing here)

## Purpose
More information about Datacenter A/S will be given on a later data
* Data Center Specs
* Facility
* ICT Servers, Routers, Switches etc
* Energy needs
* Cooling and temperature requirements
* Humidity
* What if the data center demands that the cold temperature in the server room be 32,3 then you could run free-cooling mode all year round?
* Ashrae climate data
* Benefits for PUE

## Goals
* What kind of data center is this, housing, hosting or?
* If more customers in the same data center, are there any customers who want to have their own network in the center?
* Customer requirements for certifications for data center design and or operation? Should security be blended into this?
* Access control, etc.
* How to test the data center annually?

## Schedule
The project is divided into ? phases from week 35 to week 51.

## Organization
    Supervisors
        Elias Esmati 
        Ruslan Trifonov
        Hans 
            IT-Technology
                Alexander With
                Mikkel Krogh Hansen
                Cristian Guba
                Lasse Didrichsen
                Franco Mariani Westi
                Jacob Pløger
                Jonas Offersen
                Mateusz Pietrzyk
                Nicolai Skytte
    Supervisors
        asd            
            FMS
                asd
                asd
    Supervisors
        asdas
            Computer Science
                asd
                asd
                asd
    Customers
        Datacenter A/S
            asd
        NNIT 
            asd
        Danish Datacenter Industry
            asd
        Google
            asd
    
    

## Budget and resources
Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contribute.


## Risk assessment
    Not making deadlines
    Not enough time
    Communication
    Lack of skills
    Budget
    



## Stakeholders
    Internal Stakerholders
        IT-Technology Group
        Computer Science Group
        FMS Group
    External stakeholders
        Elias Esmati 
        Ruslan Trifonov
        Hans bla bla
        Datacenter A/S
        NNIT
        Danish Datacenter Industry
        Google

## Communication
    Discord
        * Alexander With - A With#6237
        * Mikkel Krogh Hansen - Mikkel k.h.#2884
        * Cristian Guba - cristianguba#3828
        * Lasse Didrichsen - lasse#3424
        * Franco Mariani Westi - Mentle#1215
        * Jacob Pløger - Jacob Pløger#7289
        * Jonas Offersen - 
        * Mateusz Pietrzyk - pietmat2
        * Nicolai Skytte - OwlinABox #0628
    Gitlab
        * Alexander With - 
        * Mikkel Krogh Hansen - 
        * Cristian Guba -
        * Franco Mariani Westi - fran5040@edu.ucl.dk
        * Jacob Pløger - @JacobPloeger
        * Lasse Didrichsen - @lass3728
        * Jonas Offersen -
        * Mateusz Pietrzyk - 
        * Nicolai Skytte - 
    E-mail
        * Alexander With - alex8684@edu.ucl.dk
        * Mikkel Krogh Hansen - mikk744e@edu.ucl.dk
        * Cristian Guba - cristian.guba@gmail.com
        * Lasse Didrichsen - lass3728@edu.ucl.dk
        * Franco Mariani Westi - fran5040@edu.ucl.dk
        * Jacob Pløger - jaco223r@edu.ucl.dk
        * Jonas Offersen - jona415t@edu.ucl.dk
        * Mateusz Pietrzyk - mate1180@edu.ucl.dk
        * Nicolai Skytte - nico835t@edu.ucl.dk


## Perspectives


## Evaluation


## References
