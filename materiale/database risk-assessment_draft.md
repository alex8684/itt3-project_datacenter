# Datacenter project 

---
## To-do:


# Risk-assessment.

---

### Human factor
* Admins
* Supervisors
* Technicians
* Cleaning Personel 		
* Visitors
* Users
                    
---

### Physical:
* Technology Provider
* physical Connection (servers) 
	* Servers
 	* UPS
	

---            

### Software:

* Operational systems
* Encryption
* Authentication 
* Hard Drive status
* server status
* CCTV
* UPS-power
	* UPS-status
* Secure remote access
* access points
* databases
* Maintenance
        
---

### IoT:

* Unwanted/ unregistered devices
* Sensor data.
* Cooling control
* area-access(assuming via ID-card)
* Sensors
* sensor status
* Cooling/heat/(temperature)
* tempature-monitoring
	* heating/cooling
* CCTV

### PLC:

* Tempature control
* Fan control
* Automatic doors
* Emergency electrical distribution systems
* Pump control	
	* water 
* 
         
---

### Privacy:

* user 
* Admin-access
* authentication
* Customer 
* Privacy/personal data encryption

---

### Resilience:

* Operational temps
* Airflow
* Latency
* Interference  
* data exchange in system
* data exchange out of system
* physical access
* 
---

### Reliability: 

* Up-time
* Bootup time
* Maintenance 
* security

---

### Safety:

* data-protection 
* Room setup (don’t wanna get hit by a rack)

---

## WHO CAN POSSIBLY GET ACCESS TO DATA?


### Customers:

* System users
                
        
### Data Center workers:

* Cleaning personnel
* Technicians


### People from outside:

* Visitors
* supervisors
* Thieves

---

# THREATS:
* unauthorized modifications
* Faulty hardware
* technician unavailability(downtime) Maintenance 
* Structural failure
* Construction work (RIP network cables)

---

# THREAT AGENTS:
* Employees Cleaning staff
* External visitors
* Hackers
* Users
* Nature/weather/disaster/Rodents
\