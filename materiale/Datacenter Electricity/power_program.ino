int voltage = 0;
int prevVoltage = 0;
int maxVoltage = 0;
int minVoltage = 1024;
int dir = 1;
float volt = 0;

unsigned long freqTimer = 0;
unsigned long freqPrev = 0;
int period = 0;
float hertz = 0;

void setup() {
	Serial.begin(256000);
	pinMode(A0, INPUT);
	// pinMode(A1, INPUT);
}

void loop() {
	voltage = (analogRead(A0));
	if(dir == 1){
		if(voltage < prevVoltage){
			maxVoltage = prevVoltage;
			dir = -1;
		}
	}
	else if(dir == -1){
		if(voltage > prevVoltage){
			minVoltage = prevVoltage;
			dir = 1;
			freqPrev = freqTimer;
			freqTimer = millis();
			// freqCalc = true;
		}
	}
	prevVoltage = voltage;
	// int amprage = (analogRead(A1));
	
	period = freqTimer - freqPrev;
	if(period != 0){
		hertz = 1000/(period);
	}
	

	volt = volt * 0.99 + (maxVoltage - minVoltage) * 0.01;

	// Serial.print("Voltage: ");
	// Serial.print(voltage);
	// Serial.print(", ");

	// Serial.print("Amprage: ");
	// Serial.print(amprage);

	// Serial.print("MaxVoltage: ");
	// Serial.print(maxVoltage);
	// Serial.print(", ");

	// Serial.print("MinVoltage: ");
	// Serial.print(minVoltage);
	// Serial.print(", ");

	// Serial.print("Volt: ")
	Serial.print(volt);
	Serial.print(", ");

	// Serial.print("Frequency: ")
	Serial.print(hertz);
	// Serial.print(", ");


	Serial.print("\r\n");
	// delay((1000/60));
	// delay(1);
}
