---

- [ ] plan software/hardware for project

- [ ] do pros / cons

- [ ] update risk-assessment

- [ ] add vunarabillity type to risk-assessment

- [ ] implement PLC with operator/engineers

- [ ] OBC-server to PLC

- [ ] out-side consultants

- [ ] risk-matrix

- [ ] software/hardware list

- [ ] Hardware Diagram

- [ ] Software Diagram

- [ ] Network Diagram
