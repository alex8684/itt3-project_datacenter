# ITT3-Project_Datacenter

---
# Team members

	Alexander With
	Cristian Guba 
	Franco Mariani Westi
 	Jacob Pløger 
	Mateusz Pietrzyk 
	Mikkel Krogh 
	Mohammed Hussein Al Majedi  
	Nikolai Skytte 
	Jonas Offersen 
	Lasse Didrichsen 

## Datacenter Electricity

## Datacenter Cooling
---
### The goal is to make the cooling solution as efficient as possible. 
---
![overview](https://gitlab.com/alex8684/itt3-project_datacenter/-/raw/master/Documents/Plant_overview.png)

---
* Mateusz Pietrzyk
* Mikkel Krogh 
* Jacob Pløger 
* Jonas Offersen
---

## Datacenter Fan